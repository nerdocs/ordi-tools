#!/usr/bin/bash
# DEPRECATED
# migrate to Ansible

# This script installs the current version of syncthing on Ubuntu/Debian
# and starts/enables it as service

# run this file as:
# curl https://gitlab.com/nerdocs/ordi-tools/-/raw/main/setup-syncthing.sh | sh

# please select the users under which Syncthing should run
USERS="ordination diess"

# don't change anything below.
# -------------------------------------

# from https://apt.syncthing.net/
sudo curl -s -o /usr/share/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
echo "deb [signed-by=/usr/share/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
sudo apt-get update
sudo apt-get install syncthing -y

for U in $USERS; do
    sudo systemctl enable syncthing@${U}.service
    sudo systemctl start syncthing@${U}.service
done
